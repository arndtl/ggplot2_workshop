# Introduction to `ggplot2`

**Next dates:**

2 July 2018, Wissenschaftszentrum Berlin (WZB)

## Workshop description

Being able to produce meaningful and also beautiful figures is an increasingly important skill for social scientists. This course introduces you to the package ggplot2 for R. It also covers principles of good visualizations as well as "tidy data" and the "grammar of graphics" which provide the conceptual foundation of ggplot2. The objective of the workshop will be to teach you the basic knowledge needed to use ggplot2 independently, thus helping you as participant to initiate your own process of learning the specific needs of your research.

## Target group

The workshop is aimed at doctoral researchers in the social sciences who posses a basic familiarity with R.

## Workshop Content and Goals

The workshop will include some structured lectures, but emphasizes practical exercises. Data and code will be provided for each exercise but participants are encouraged to use their own data.

The workshop will cover:

- "tidy data"
- basics of dplyr and tidyr
- "grammar of graphics"
- ggplot2

## Prior Knowledge and Preparation

You will use your own laptops as R and RStudio are available free of charge for all major operating systems - one of the many benefits of R. You will be expected to have installed R and RStudio prior to coming to the workshop. If you experience any problems installing R, RStudio or both do not hesitate to contact me before coming to the workshop.

You are invited to bring datasets as well as figures and tables from current or past projects to the workshop. If possible, please send them to me before the workshop. In the final session of the workshop we will discuss one of your figures and whether and how it could be improved. You will then have time to rebuild your figure with ggplot2.

If you wish to brush up your R skills before the workshop I suggest you complete either the "Try R" (Codeschool) or "Introduction to R" (Datacamp). Both courses are free, require only and are interactive. You have to register an account but this and the referenced courses are free of charge.

```
Use install_packages.R to install all packages that will be used in this workshop.
```

## Workshop program

### Monday, 2 July 2018

9:30 am - 10:45 am Welcome, brief recap of foundations in R and introduction to "tidy" data using `dplyr` and `tidyr`

*10:45 am - 11:00 am Coffee Break*

11:15 - 12:30 pm Introduction to the "Grammar of Graphics" and `ggplot2`

*12:30 pm - 1:30 pm Lunch break*

1:30 pm - 2:45 pm Scales, Coordinates, Facets and Themes

*2:45pm - 3:00 pm Coffee Break*

3:00 pm - 4:30pm Principles of good visualizations and 'Plot it yourself'

## Your instructor

Arndt Leininger is a research fellow at the Chair for Political Sociology of Germany at Freie Universität Berlin. His areas of research are in comparative politics and applied quantitative methods. He is interested in direct democracy, turnout, election forecasting, economic voting and electoral studies more broadly. Arndt has recently received his PhD from the Hertie School of Governance, Berlin, and holds an MSc in Political Science and Political Economy from the London School of Economics and Political Science.

## References

### On-line

[Cookbook for R](): Solutions to common tasks and problems in analyzing data.

Particularly helpful when it comes to ggplot2.

Try R An interactive R tutorial by Code School. The course is free but requires registration.

Introduction to R An interactive online course by DataCamp. The course is free but requires registration.

Advanced R Free version of Hadley Wickham’s “Advanced R” book.


### Off-line

Wickham, H. (2009): ggplot2. Elegant Graphics for Data Analysis.

For further reading a list of publications related to R is also available on CRAN (Comprehensive R Archive Network).
